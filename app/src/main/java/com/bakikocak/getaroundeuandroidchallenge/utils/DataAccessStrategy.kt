package com.bakikocak.getaroundeuandroidchallenge.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import kotlinx.coroutines.Dispatchers

/**
 * First, we let our LiveData know that we are looking for the data, so that it should have a LOADING state.
 * Then, we try to get the data from the local database since it would be faster. If there is data in local db, we are changing the state to a SUCCESS
 * Regardless of the result of the local database operation, we are fetching the data from the internet as well just to keep our app synced. (useful in case that there would be new data in the servet)
 * (note that the ui thread won’t be blocked and the user could already be shown the correct car data as it is firstly fetched from db rapidly).
 * Finally, we need to save the result from the network call in the database, in order to keep it updated.
 */

fun <T, L> performGetOperation(
        databaseQuery: () -> LiveData<T>,
        networkCall: suspend () -> ResultData<L>,
        saveCallResult: suspend (L) -> Unit
): LiveData<ResultData<T>> =
        liveData(Dispatchers.IO) {
            emit(ResultData.loading(null))
            val source = databaseQuery().map { ResultData.success(it) }
            emitSource(source)

            when (val responseStatus = networkCall.invoke()) {
                is ResultData.Success -> {
                    saveCallResult(responseStatus.value)
                }

                is ResultData.Failure -> {
                    emit(ResultData.failure(responseStatus.message))
                    emitSource(source)
                }

                else -> {
                    emit(ResultData.failure("Something went wrong, please try again later"))
                    emitSource(source)
                }
            }
        }