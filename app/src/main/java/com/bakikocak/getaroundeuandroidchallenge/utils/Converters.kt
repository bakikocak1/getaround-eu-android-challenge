package com.bakikocak.getaroundeuandroidchallenge.utils

import androidx.room.TypeConverter
import com.bakikocak.getaroundeuandroidchallenge.data.model.Owner
import com.bakikocak.getaroundeuandroidchallenge.data.model.Rating
import com.google.gson.Gson

/**
 *  Converter to write Rating and Owner classes to database.
 */
class Converters {

    @TypeConverter
    fun fromRating(rating: Rating): String = Gson().toJson(rating)

    @TypeConverter
    fun toRating(string: String): Rating = Gson().fromJson(string, Rating::class.java)

    @TypeConverter
    fun fromOwner(owner: Owner): String = Gson().toJson(owner)

    @TypeConverter
    fun toOwner(string: String): Owner = Gson().fromJson(string, Owner::class.java)

}