package com.bakikocak.getaroundeuandroidchallenge

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GetaroundApp : Application()
