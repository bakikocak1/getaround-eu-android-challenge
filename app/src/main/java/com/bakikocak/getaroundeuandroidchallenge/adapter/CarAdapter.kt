package com.bakikocak.getaroundeuandroidchallenge.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bakikocak.getaroundeuandroidchallenge.R
import com.bakikocak.getaroundeuandroidchallenge.data.model.CarItem
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.car_item_layout.view.*

class CarAdapter : RecyclerView.Adapter<CarAdapter.ViewHolder>() {

    // ViewHolder class
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    // DiffUtil implementation
    private val differCallback = object : DiffUtil.ItemCallback<CarItem>() {
        override fun areItemsTheSame(oldItem: CarItem, newItem: CarItem): Boolean =
                oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: CarItem, newItem: CarItem): Boolean =
                oldItem == newItem
    }

    val differ = AsyncListDiffer(this, differCallback)


    // Adapter methods overridden
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                LayoutInflater.from(parent.context).inflate(
                        R.layout.car_item_layout,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var car = differ.currentList[position]

        holder.itemView.apply {

            val brand = car.brand
            val model = car.model
            val price = car.price_per_day.toString().plus(" ").plus("\u20ac/j")

            Glide.with(this).load(car.picture_url).into(car_thumbnail_iv)

            car_brand_model_tv.text = "$brand $model"
            car_price_tv.text = price
            car_rating_rb.rating = car.rating.average
            car_rating_count_tv.text = car.rating.count.toString()

            setOnClickListener {
                onItemClickListener?.let { it(car) }
            }
        }
    }

    override fun getItemCount(): Int = differ.currentList.size

    private var onItemClickListener: ((CarItem) -> Unit)? = null

    fun setOnItemClickListener(listener: (CarItem) -> Unit) {
        onItemClickListener = listener
    }
}