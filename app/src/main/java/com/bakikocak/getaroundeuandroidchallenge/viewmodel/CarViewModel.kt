package com.bakikocak.getaroundeuandroidchallenge.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.bakikocak.getaroundeuandroidchallenge.data.model.CarItem
import com.bakikocak.getaroundeuandroidchallenge.repository.CarRepository

import com.bakikocak.getaroundeuandroidchallenge.utils.ResultData

class CarViewModel @ViewModelInject constructor(
        private val carRepository: CarRepository) : ViewModel() {

    private val carList = carRepository.getCarList()  // get list data

    fun getCarList(): LiveData<ResultData<List<CarItem>>> = carList  // observe in list fragment

    fun getSingleCar(id: Int) = carRepository.getSingleCar(id) // observe in detail fragment
}