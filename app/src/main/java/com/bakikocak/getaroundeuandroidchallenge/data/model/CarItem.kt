package com.bakikocak.getaroundeuandroidchallenge.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cars")
data class CarItem(
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        val brand: String,
        val model: String,
        val owner: Owner,
        val picture_url: String,
        val price_per_day: Int,
        val rating: Rating
)