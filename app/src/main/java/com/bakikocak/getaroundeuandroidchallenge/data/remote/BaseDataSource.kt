package com.bakikocak.getaroundeuandroidchallenge.data.remote

import com.bakikocak.getaroundeuandroidchallenge.utils.ResultData
import retrofit2.Response

/**
 *  getData method to encapsulate the Retrofit response in a Resource.
 */

abstract class BaseDataSource {

    protected suspend fun <T> getData(call: suspend () -> Response<T>): ResultData<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return ResultData.success(body)
            }
            return formatError(" ${response.code()} ${response.message()}")
        } catch (exception: Exception) {
            return formatError(exception.message ?: exception.toString())
        }
    }

    private fun <T> formatError(errorMessage: String): ResultData<T> {
        return ResultData.failure("Network call has failed for a following reason: $errorMessage")
    }

}