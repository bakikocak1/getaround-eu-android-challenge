package com.bakikocak.getaroundeuandroidchallenge.data.remote

import javax.inject.Inject

/**
 *  Remote data source to make network call through Retrofit service.
 */
class CarRemoteDataSource @Inject constructor(
        private val carService: CarService
) : BaseDataSource() {

    suspend fun getCarList() = getData { carService.getCarList() }
}