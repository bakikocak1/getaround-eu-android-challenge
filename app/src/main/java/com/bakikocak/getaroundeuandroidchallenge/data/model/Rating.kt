package com.bakikocak.getaroundeuandroidchallenge.data.model

data class Rating(
        val average: Float,
        val count: Int
)