package com.bakikocak.getaroundeuandroidchallenge.data.remote

import com.bakikocak.getaroundeuandroidchallenge.data.model.CarResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 *  A service interface to be used by Retrofit calls.
 */
interface CarService {

    @GET("drivy/jobs/master/android/api/cars.json")
    suspend fun getCarList(): Response<CarResponse>
}