package com.bakikocak.getaroundeuandroidchallenge.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bakikocak.getaroundeuandroidchallenge.data.model.CarItem
import com.bakikocak.getaroundeuandroidchallenge.utils.Converters

/**
 * Database to save carItem data locally.
 */

@Database(entities = [CarItem::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun carsDao(): CarsDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
                instance ?: synchronized(this) {
                    instance ?: buildDatabase(context).also { instance = it }
                }

        private fun buildDatabase(appContext: Context) =
                Room.databaseBuilder(appContext, AppDatabase::class.java, "cars_db")
                        .fallbackToDestructiveMigration()
                        .build()
    }
}