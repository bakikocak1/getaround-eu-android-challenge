package com.bakikocak.getaroundeuandroidchallenge.data.model


data class Owner(
        val name: String,
        val picture_url: String,
        val rating: Rating
)