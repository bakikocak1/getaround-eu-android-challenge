package com.bakikocak.getaroundeuandroidchallenge.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bakikocak.getaroundeuandroidchallenge.data.model.CarItem

/**
 *  Dao to perform operations over the database.
 */
@Dao
interface CarsDao {

    @Query("SELECT * FROM cars")
    fun getAllCars(): LiveData<List<CarItem>>

    @Query("SELECT * FROM cars WHERE id = :id")
    fun getSingleCar(id: Int): LiveData<CarItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(cars: List<CarItem>)

}