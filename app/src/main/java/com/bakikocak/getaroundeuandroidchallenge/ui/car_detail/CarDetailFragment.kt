package com.bakikocak.getaroundeuandroidchallenge.ui.car_detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bakikocak.getaroundeuandroidchallenge.R
import com.bakikocak.getaroundeuandroidchallenge.data.model.CarItem
import com.bakikocak.getaroundeuandroidchallenge.viewmodel.CarViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_car_detail.*


@AndroidEntryPoint
class CarDetailFragment : Fragment() {

    private val viewModel: CarViewModel by activityViewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val id = requireArguments().getInt("car_id")

        // Since we do not have any api endpoint for car detail, I fetched single car data from the data saved in Room DB before instead of passing whole carItem data.
        viewModel.getSingleCar(id).observe(viewLifecycleOwner, {
            it.apply {
                hideProgressBar()
                setupUI(it)
            }
        })

        showProgressBar()
    }

    private fun setupUI(car: CarItem) {
        // Car Details
        Glide.with(this)
                .load(car.picture_url)
                .into(car_detail_thumbnail_iv)

        val brand = car.brand
        val model = car.model
        val price = car.price_per_day.toString().plus(" ").plus("\u20ac/j")

        car_detail_brand_model_tv.text = "$brand $model"
        car_detail_price_tv.text = price
        car_detail_rating_tv.rating = car.rating.average

        car_detail_rating_count_tv.text = car.rating.count.toString()

        // Owner Details
        Glide.with(this)
                .load(car.owner.picture_url)
                .into(owner_profile_iv)

        tv_owner_name.text = car.owner.name

        owner_rating_bar.rating = car.owner.rating.average

        owner_rating_count_tv.text = car.owner.rating.count.toString()
    }

    private fun showProgressBar() {
        detail_progress_bar.visibility = VISIBLE
        detail_data_layout.visibility = GONE
    }

    private fun hideProgressBar() {
        detail_progress_bar.visibility = GONE
        detail_data_layout.visibility = VISIBLE
    }
}