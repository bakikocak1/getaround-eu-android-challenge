package com.bakikocak.getaroundeuandroidchallenge.ui.cars_list

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.bakikocak.getaroundeuandroidchallenge.R
import com.bakikocak.getaroundeuandroidchallenge.adapter.CarAdapter
import com.bakikocak.getaroundeuandroidchallenge.utils.ResultData
import com.bakikocak.getaroundeuandroidchallenge.viewmodel.CarViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_car_list.*

@AndroidEntryPoint
class CarListFragment : Fragment() {

    lateinit var navController: NavController
    lateinit var carAdapter: CarAdapter

    private val viewModel: CarViewModel by activityViewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

        setupCarRv()

        // Navigate to detail fragment on adapter item click
        carAdapter.setOnItemClickListener {
            val bundle = bundleOf("car_id" to it.id)
            navController.navigate(R.id.action_carListFragment_to_carDetailFragment, bundle)
        }

        // Observe the car list
        viewModel.getCarList().observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is ResultData.Success -> {
                    hideProgressBar()
                    response.value.let { carResponse ->
                        carAdapter.differ.submitList(carResponse)
                    }
                }

                is ResultData.Failure -> {
                    hideProgressBar()
                    response.message.let { message ->
                        showError(message)
                    }
                }

                is ResultData.Loading -> {
                    showProgressBar()
                }
            }
        })
    }

    private fun showProgressBar() {
        list_progress_bar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        list_progress_bar.visibility = View.GONE
    }

    private fun showError(message: String) {
        val errorBar = Snackbar.make(list_fragment_root_layout, message, Snackbar.LENGTH_INDEFINITE)
        errorBar.setAction("Try again") { viewModel.getCarList() }
        errorBar.show()
    }

    private fun setupCarRv() {
        carAdapter = CarAdapter()
        car_list_rv.apply {
            adapter = carAdapter
            layoutManager = GridLayoutManager(activity, getSpanCount())
        }
    }

    private fun getSpanCount(): Int =
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) 1 else 2
}
