package com.bakikocak.getaroundeuandroidchallenge.di

import com.bakikocak.getaroundeuandroidchallenge.data.local.CarsDao
import com.bakikocak.getaroundeuandroidchallenge.data.remote.CarRemoteDataSource
import com.bakikocak.getaroundeuandroidchallenge.data.remote.CarService
import com.bakikocak.getaroundeuandroidchallenge.repository.CarRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideCarRepository(
            carRemoteDataSource: CarRemoteDataSource,
            carLocalDataSource: CarsDao
    ): CarRepository {
        return CarRepository(carRemoteDataSource, carLocalDataSource)
    }

    @Singleton
    @Provides
    fun provideCharacterRemoteDataSource(carService: CarService) = CarRemoteDataSource(carService)
}