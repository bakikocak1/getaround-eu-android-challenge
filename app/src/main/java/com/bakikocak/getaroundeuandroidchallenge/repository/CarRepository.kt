package com.bakikocak.getaroundeuandroidchallenge.repository

import com.bakikocak.getaroundeuandroidchallenge.data.local.CarsDao
import com.bakikocak.getaroundeuandroidchallenge.data.remote.CarRemoteDataSource
import com.bakikocak.getaroundeuandroidchallenge.utils.performGetOperation
import javax.inject.Inject

/**
 *  Repository to get data from remote/local sources.
 */
class CarRepository @Inject constructor(
        private val remoteDataSource: CarRemoteDataSource,
        private val localDataSource: CarsDao
) {

    // Load car list from DB, if does not exist,
    fun getCarList() = performGetOperation(
            databaseQuery = { localDataSource.getAllCars() },
            networkCall = { remoteDataSource.getCarList() },
            saveCallResult = { localDataSource.insertAll(it) })

    // Note that there is no api endpoint for single car, it is fetched from only DB.
    fun getSingleCar(id: Int) = localDataSource.getSingleCar(id)

}