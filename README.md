# Getaround EU Android Challenge #

This README would normally document whatever steps are necessary to get your application up and running.

### Tech Stack: ###

* Kotlin
* MVVM architecture
* Jetpack Architecture Components
* Kotlin Coroutines - LiveData
* Retrofit - Room DB

### How do I get set up? ###

* clone this repo via terminal : git clone https://bakikocak1@bitbucket.org/bakikocak1/getaround-eu-android-challenge.git


### Contact ###

Baki Kocak
* e-mail: abdulbakikocak@gmail.com
